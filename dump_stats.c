#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "sql_serv.h"
#include "dump_stats.h"

static int dump_activated = 0;

static FILE *overall_stats = NULL;
static FILE *committer_stats = NULL;
static FILE *path_stats = NULL;
static FILE *overall_header = NULL;

static FILE *top_commits = NULL;
static FILE *top_changes = NULL;
static FILE *top_addition = NULL;
static FILE *top_deletion = NULL;

void
dump_stats_commit(rs_commit_t *rs)
{
    if (!dump_activated)
        return;

    fprintf(overall_header, "<br>First Commit<br>");
    fprintf(overall_header, "============<br>");

    fprintf(overall_header, "Author: %s<br>", rs->author);
    fprintf(overall_header, "Date: %s<br>", mkdate(rs->date));
    fprintf(overall_header, "Hash: %s<br>", rs->hash);
    fprintf(overall_header, "Addition: %d<br>", rs->addition);
    fprintf(overall_header, "Deletion: %d<br>", rs->deletion);
    fprintf(overall_header, "Changes: %d<br>", rs->changes);

    int i, max;

    fprintf(overall_header, "<br>Files:<br>");
    fprintf(overall_header, "============<br>");
    max = 10;
    if (rs->changes < max)
        max = rs->changes;
    for (i = 0; i < max; ++i) {
        fprintf(overall_header, "%s<br>", rs->files[i]);
    }
    if (rs->changes > 10)
        fprintf(overall_header, "......<br>");
}

static void
print_tops(rs_top_t *top, int len, int total, FILE *fp)
{
    int i, sum = 0;

    fprintf(fp, "label,value\n");

    for (i = 0; i < len; ++i, ++top) {
        fprintf(fp, "%s,%d\n", top->name, top->stat);
        sum += top->stat;
    }
    fprintf(fp, "others,%d\n", total - sum);
}

static void
print_overall_stat(rs_overall_t *rs)
{
    fprintf(overall_header, "<br>Overall statistics<br>");
    fprintf(overall_header, "==================<br>");
    fprintf(overall_header, "Commits: %d<br>", rs->commits);
    fprintf(overall_header, "Changes: %d<br>", rs->changes);
    fprintf(overall_header, "Files: %d<br>", rs->files);
    fprintf(overall_header, "Authors: %d<br>", rs->authors);
    fprintf(overall_header, "Addition: %d<br>", rs->addition);
    fprintf(overall_header, "Deletion: %d<br>", rs->deletion);

    fprintf(overall_header, "<br>Biggest commit<br>");
    fprintf(overall_header, "==============<br>");
    fprintf(overall_header, "Author: %s<br>", rs->biggest_commit_author);
    fprintf(overall_header, "Date: %s<br>", mkdate(rs->biggest_commit_time));
    fprintf(overall_header, "Hash: %s<br>", rs->biggest_commit_hash);
    fprintf(overall_header, "Addition: %d<br>", rs->biggest_commit_addition);
    fprintf(overall_header, "Deletion: %d<br>", rs->biggest_commit_deletion);

    print_tops(rs->most_commits, QUERY_LIMIT, rs->commits, top_commits);
    print_tops(rs->most_changes, QUERY_LIMIT, rs->changes, top_changes);
    print_tops(rs->most_addition, QUERY_LIMIT, rs->deletion, top_addition);
    print_tops(rs->most_deletion, QUERY_LIMIT, rs->deletion, top_deletion);
}

void
dump_stats_overall(int year, int season, rs_overall_t *rs)
{
    if (!dump_activated)
        return;

    if (year && season) {
        fprintf(overall_stats, "%d-Q%d,%d,%d,%d,%d,%d,%d,",
                year, season, rs->commits, rs->changes,
                rs->files, rs->authors, rs->addition,
                rs->deletion);
    } else {
        print_overall_stat(rs);
        return;
    }

    fprintf(overall_stats, "Biggest commit<br>");
    fprintf(overall_stats, "==============<br>");
    fprintf(overall_stats, "Author: %s<br>", rs->biggest_commit_author);
    fprintf(overall_stats, "Date: %s<br>", mkdate(rs->biggest_commit_time));
    fprintf(overall_stats, "Hash: %s<br>", rs->biggest_commit_hash);
    fprintf(overall_stats, "Addition: %d<br>", rs->biggest_commit_addition);
    fprintf(overall_stats, "Deletion: %d<br>", rs->biggest_commit_deletion);

    fprintf(overall_stats, "\n");
}

void
dump_stats_overall_header(void)
{
    if (!dump_activated)
        return;
    fprintf(overall_stats,
        "season,commits,changes,files,authors,addition,deletion,comments\n");
}

void
dump_stats_overall_tail(void)
{
    if (!dump_activated)
        return;
}

static void
print_committer_top(rs_author_t *rs, FILE *fp)
{
    int i;

    fprintf(fp, "Top files of addition<br>");
    fprintf(fp, "=====================<br>");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs->most_addition[i].name[0])
            fprintf(fp, "%s: %d<br>", rs->most_addition[i].name, rs->most_addition[i].stat);
    }

    fprintf(fp, "<br><br>Top files of change<br>");
    fprintf(fp, "===================<br>");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs->most_change[i].name[0])
            fprintf(fp, "%s: %d<br>", rs->most_change[i].name, rs->most_change[i].stat);
    }
}

void
dump_stats_committer(int year, int season, rs_author_t *rs)
{
    if (!dump_activated)
        return;

    if (year && season) {
        fprintf(committer_stats, "%d-Q%d,%d,%d,%d,%d,",
                year, season, rs->commits, rs->changes,
                rs->addition, rs->deletion);
    } else {
        fprintf(overall_header, "Overall<br>");
        fprintf(overall_header, "=======<br>");

        fprintf(overall_header, "Commits: %d<br>", rs->commits);
        fprintf(overall_header, "Changes: %d<br>", rs->changes);
        fprintf(overall_header, "Addition: %d<br>", rs->addition);
        fprintf(overall_header, "Deletion: %d<br><br>", rs->deletion);

        print_committer_top(rs, overall_header);
        fprintf(overall_header, "<br>");

        return;
    }


    print_committer_top(rs, committer_stats);
    fprintf(committer_stats, "\n");
}

void
dump_stats_committer_head(void)
{
    if (!dump_activated)
        return;
    fprintf(committer_stats,
        "season,commits,changes,addition,deletion,comments\n");
}

void
dump_stats_committer_tail(void)
{
}

void
dump_stats_path_head(void)
{
    if (!dump_activated)
        return;
    fprintf(path_stats,
        "season,authors,commits,changes,addition,deletion,files,comments\n");
}

void
dump_stats_path_tail(void)
{
}

static void
print_path_top(rs_file_t *rs, FILE *fp)
{
    int i;

    fprintf(path_stats, "Top committer of addition<br>");
    fprintf(path_stats, "=========================<br>");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs->top_committers[i].name[0])
            fprintf(path_stats, "%s: %d<br>", rs->top_committers[i].name, rs->top_committers[i].stat);
    }

    fprintf(path_stats, "<br>Top files of change<br>");
    fprintf(path_stats, "===================<br>");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs->top_changes[i].name[0])
            fprintf(path_stats, "%s: %d<br>", rs->top_changes[i].name, rs->top_changes[i].stat);
    }
}

void
dump_stats_path(int year, int season, rs_file_t *rs)
{
    if (!dump_activated)
        return;

    if (year && season) {
        fprintf(path_stats, "%d-Q%d,%d,%d,%d,%d,%d,%d,",
                year, season, rs->authors, rs->commits, rs->changes,
                rs->addition, rs->deletion, rs->files);
    } else {
        fprintf(overall_header, "Overall<br>");
        fprintf(overall_header, "=======<br>");

        fprintf(overall_header, "Authors: %d<br>", rs->authors);
        fprintf(overall_header, "Commits: %d<br>", rs->commits);
        fprintf(overall_header, "Changes: %d<br>", rs->changes);
        fprintf(overall_header, "Addition: %d<br>", rs->addition);
        fprintf(overall_header, "Deletion: %d<br>", rs->deletion);
        fprintf(overall_header, "Files: %d<br>", rs->files);

        print_tops(rs->top_committers, QUERY_LIMIT, rs->addition, top_addition);
        print_tops(rs->top_changes, QUERY_LIMIT, rs->changes, top_changes);

        return;
    }


    print_path_top(rs, path_stats);
    fprintf(path_stats, "\n");
}

static int
init_file(FILE **fpp, const char *path, const char *name)
{
    static char full_path[1024];
    FILE *fp;

    sprintf(full_path, "./%s/%s", path, name);
    fp = fopen(full_path, "w");
    if (!fp) {
        fprintf(stderr, "Warning: cannot create file %s\n", full_path);
        return -1;
    }
    *fpp = fp;
    return 0;
}

void
dump_stats_init(const char *path)
{
    int rc = mkdir(path, S_IRUSR | S_IWUSR | S_IXUSR);

    if (rc == -1) {
        fprintf(stderr, "Warning: cannot create path %s\n", path);
        return;
    }

    if (init_file(&overall_stats, path, "overall_stats.csv"))
        return;
    if (init_file(&committer_stats, path, "committer_stats.csv"))
        return;
    if (init_file(&overall_header, path, "overall_header.txt"))
        return;
    if (init_file(&top_commits, path, "top_commits.csv"))
        return;
    if (init_file(&top_changes, path, "top_changes.csv"))
        return;
    if (init_file(&top_addition, path, "top_addition.csv"))
        return;
    if (init_file(&top_deletion, path, "top_deletion.csv"))
        return;
    if (init_file(&path_stats, path, "path_stats.csv"))
        return;

    dump_activated = 1;
}

void
dump_stats_fini(void)
{
    if (dump_activated) {
        dump_activated = 0;
        fclose(overall_stats);
        fclose(committer_stats);
        fclose(overall_header);
        fclose(top_commits);
        fclose(top_changes);
        fclose(top_addition);
        fclose(top_deletion);
        fclose(path_stats);
    }
}

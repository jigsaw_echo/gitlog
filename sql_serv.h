#ifndef _SQL_SERV_H
#define _SQL_SERV_H

#include <time.h>

#include "config.h"

#define QUERY_LIMIT 5
#define QUERY_LIMIT_S "5"

struct sql_server;
typedef struct sql_server sql_serv_t;

typedef struct {
    int id;
    int is_binary;
    char path[MAX_PATH];
    int addition;
    int deletion;
    int changes;
} rec_file_t;

typedef struct {
    int id;
    char name[MAX_AUTHOR];
    int commits;
    int changes;
    int addition;
    int deletion;
} rec_author_t;

typedef struct {
    int id;
    int author_id;
    char hash[MAX_HASH];
    unsigned int date;
    int changes;
    int addition;
    int deletion;
} rec_commit_t;

typedef struct {
    int id;
    int commit_id;
    int file_id;
    int author_id;
    int addition;
    int deletion;
    unsigned int date;
} rec_change_t;

extern int lookup_author(sql_serv_t *serv, const char *name, rec_author_t *rec);
extern int insert_author(sql_serv_t *serv, rec_author_t *rec);
extern int update_author(sql_serv_t *serv, rec_author_t *rec);
extern int lookup_file(sql_serv_t *serv, char *path, rec_file_t *rec);
extern int insert_file(sql_serv_t *serv, rec_file_t *rec);
extern int update_file(sql_serv_t *serv, rec_file_t *rec);
extern int insert_commit(sql_serv_t *serv, rec_commit_t *rec);
extern int insert_change(sql_serv_t *serv, rec_change_t *rec);

extern sql_serv_t *sql_serv_init(int create, char *db);
extern void sql_serv_fini(sql_serv_t *);
extern int sql_serv_transaction_begin(sql_serv_t *);
extern int sql_serv_transaction_commit(sql_serv_t *);


/* Statistics for given period for:
 *
 * - Commits
 * - Changes
 * - Addition
 * - Deletion
 * - Number of authors
 * - QUERY_LIMIT Authors with most commits
 * - QUERY_LIMIT Authors with most changes
 * - QUERY_LIMIT Authors with most lines (addition + deletion)
 *
 */

typedef struct {
    char name[MAX_STRING];
    int stat;
} rs_top_t;

typedef struct {
    char author[MAX_AUTHOR];
    char hash[MAX_HASH];
    unsigned int date;
    int addition;
    int deletion;
    int changes;
    char *files[0];
} rs_commit_t;

typedef struct {
    int commits;
    int changes;
    int files;
    int authors;
    int addition;
    int deletion;

    char biggest_commit_hash[MAX_HASH];
    int biggest_commit_addition;
    int biggest_commit_deletion;
    char biggest_commit_author[MAX_AUTHOR];
    int biggest_commit_time;

    rs_top_t most_commits[QUERY_LIMIT];
    rs_top_t most_changes[QUERY_LIMIT];
    rs_top_t most_addition[QUERY_LIMIT];
    rs_top_t most_deletion[QUERY_LIMIT];
} rs_overall_t;

extern int
query_overall(sql_serv_t *serv,
          unsigned int start,
          unsigned int end,
          rs_overall_t *rec);

extern void
query_epoch_time(sql_serv_t *serv, time_t *begin, time_t *end);

typedef struct {
    int commits;
    int changes;
    int addition;
    int deletion;
    rs_top_t most_addition[QUERY_LIMIT];
    rs_top_t most_change[QUERY_LIMIT];
} rs_author_t;

extern int
query_author(sql_serv_t *serv,
             unsigned int start,
             unsigned int end,
             const char *name,
             rs_author_t *rs);

typedef struct {
    int authors;
    int commits;
    int changes;
    int addition;
    int deletion;
    int files;
    rs_top_t top_committers[QUERY_LIMIT];
    rs_top_t top_changes[QUERY_LIMIT];
} rs_file_t;

/* the file(s) starts with given path */
extern int
query_file(sql_serv_t *serv,
           unsigned int start,
           unsigned int end,
           const char *path,
           rs_file_t *rs);

extern rs_commit_t *
query_init_commit(sql_serv_t *serv, const char *name);

extern void
free_rs_commit(rs_commit_t *);

#endif

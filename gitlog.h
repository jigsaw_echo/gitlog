#ifndef _GIT_LOG_H
#define _GIT_LOG_H

#include <stdio.h>
#include "config.h"

struct git_change;
typedef struct git_change git_change_t;

struct git_change {
    char    path[MAX_PATH];
    int     is_binary;
    int     addition;
    int     deletion;
    struct git_change *next;
};

struct git_commit;
typedef struct git_commit git_commit_t;

struct git_commit {
    unsigned int    date;
    char        hash[MAX_HASH];
    char        author[MAX_AUTHOR];
    int         change;
    int         addition;
    int         deletion;
    struct git_change *changes;
};

typedef int (*add_commit_t)(git_commit_t *, void *);

extern int parse_gitlog(FILE *, add_commit_t, void *);

#endif

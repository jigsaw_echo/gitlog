#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE
#endif

#define _GNU_SOURCE

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gitlog.h"


/* for debugging */
static int curr_line_num = 0;

#define MAX_LINE 2048
static char *line_buf = NULL;

static int line_buffered = 0;

static void
free_changes(struct git_change *h)
{
    struct git_change *n;

    while (h) {
        n = h->next;
        free(h);
        h = n;
    }
}

#define next_line(__ret) do { \
    line = line_buf; \
    len = MAX_LINE; \
    if (line_buffered) { \
        n = strlen(line); \
        len = n; \
        line_buffered = 0; \
        break; \
    } \
    ++curr_line_num; \
    if ((n = getline(&line, &len, fp)) == -1) { \
        line_buf = line; \
        return (__ret); \
    } \
    line_buf = line; \
} while (0)

/* git log --no-merges --format="%n%H::%cn::%ct" --numstat */

static int
read_commit(FILE *fp, struct git_commit *cm)
{

    /* format of commit is as below
       dc5c98671529d75e5bfe8057eccc123797949da4::Qinglai Xiao::1431636850
     */
    size_t len = 0;
    ssize_t n = 0;
    char *line = NULL;
    char *p, *q;

    memset(cm, 0, sizeof(*cm));

    next_line(-1);

    /* skip empty lines */
    while (n == 0 || (n == 1 && line[0] == '\n')) {
        next_line(-1);
    }

    p = line;
    /* first 40 char is hash */
    memcpy(cm->hash, p, 40); 
    p += 40;

    /* skip :: */
    p += 2;
    q = p;
    while (*q != ':')
        ++q;
    /* name */
    memcpy(cm->author, p, q - p);

    /* skip :: */
    p = q + 2;
    cm->date = strtol(p, NULL, 10);

    /* skip blank line */
    next_line(-1);

    return 0;
}

static int
is_change_line(const char *s)
{
    if (s[0] == '-' && s[1] == '\t')
        return 1;

    char *end = NULL;
    int a = strtoul(s, &end, 10);

    if (a >= 0 && end[0] == '\t')
        return 1;
    return 0;
}

static struct git_change *
read_change(FILE *fp)
{
    /* format of change is one line as below

        1\t1\tpath

     */
    size_t len = 0;
    ssize_t n = 0;
    char *line = NULL;

    next_line(NULL);

    if (n == 0 || (n == 1 && line[0] == '\n'))
        return NULL;

    /* check if this is a change line. there are empty commits. */
    if (!is_change_line(line)) {
        line_buffered = 1;
        return NULL;
    }

    struct git_change *ch = calloc(sizeof(*ch), 1);
    int add, del;
    char *p = NULL;

    if (line[0] == '-') {
        ch->is_binary = 1;
    } else {
        sscanf(line, "%d\t%d\t%s\n", &ch->addition, &ch->deletion, ch->path);
    }

    return ch;
}

int
parse_gitlog(FILE *fp, add_commit_t cb, void *arg)
{
    struct git_commit cm;

    if (!line_buf)
        line_buf = malloc(MAX_LINE);

    curr_line_num = 0;

    do {
        struct git_change *head, *tail, *c;

        if (read_commit(fp, &cm) == -1)
            break;

        head = tail = NULL;

        while ((c = read_change(fp))) {
            if (!head)
                head = c;
            if (tail)
                tail->next = c;
            tail = c;
            ++cm.change;
            cm.addition += c->addition;
            cm.deletion += c->deletion;
        }

        cm.changes = head;
        if (cb(&cm, arg) == -1) {
            free_changes(head);
            return -1;
        }
        free_changes(head);

    } while (1);

    free(line_buf);
    line_buf = NULL;

    return 0;
}

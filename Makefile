CC=gcc
LD=gcc

CFLAGS = -Wall -Wno-unused -Werror -I. -g -O0
#CFLAGS += -DDEBUG
LDFLAGS = -lsqlite3

#AddressSanitizer is introduced in GCC 4.8
#CFLAGS += -fsanitize=address -fno-omit-frame-pointer
#LDFLAGS += -fsanitize=address

TARGET = gitlog 

#LDFLAGS += -pg
#CFLAGS += -pg

DEPS = gitlog.h sql_serv.h config.h dump_stats.h
OBJ = main.o gitlog.o sql_serv.o dump_stats.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(TARGET): $(OBJ)
	$(LD) -o $@ $^ $(LDFLAGS)

.PHONY: clean

clean:
	rm -f $(OBJ) $(TARGET)

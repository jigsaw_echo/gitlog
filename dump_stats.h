#ifndef _DUMP_STATS_H
#define _DUMP_STATS_H

#include <time.h>
#include "sql_serv.h"

extern void dump_stats_commit(rs_commit_t *rs);
extern void dump_stats_overall(int year, int season, rs_overall_t *rs);
extern void dump_stats_overall_header(void);
extern void dump_stats_overall_tail(void);
extern void dump_stats_committer(int year, int season, rs_author_t *rs);
extern void dump_stats_committer_head(void);
extern void dump_stats_committer_tail(void);
extern void dump_stats_init(const char *path);
extern void dump_stats_fini(void);
extern void dump_stats_path_head(void);
extern void dump_stats_path_tail(void);
extern void dump_stats_path(int year, int season, rs_file_t *rs);

extern const char *mkdate(time_t t);

#endif

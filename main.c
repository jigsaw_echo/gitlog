#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 600
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>

#include "sql_serv.h"
#include "gitlog.h"
#include "dump_stats.h"

static int
add_commit(git_commit_t *cm, void *arg)
{
    sql_serv_t *s = arg;

    /* lookup author by name; if not exists insert new author */
    rec_author_t au;
    int rc;

    rc = lookup_author(s, cm->author, &au);
    if (rc == -1)
        goto out;
    if (rc == 0) {
        memset(&au, 0, sizeof(au));
        memcpy(au.name, cm->author, MAX_AUTHOR);
        if (insert_author(s, &au) == -1)
            goto out;
    }

    /* insert commit */
    rec_commit_t co;

    co.author_id = au.id;
    memcpy(co.hash, cm->hash, MAX_HASH);
    co.date = cm->date;
    co.changes = cm->change;
    co.addition = cm->addition;
    co.deletion = cm->deletion;

    if (insert_commit(s, &co) == -1)
        goto out;

    /* iterate changes */
    struct git_change *ch = cm->changes;

    while (ch) {
        /* lookup file by path; it not exists insert new file */
        rec_file_t rf;

        rc = lookup_file(s, ch->path, &rf);
        if (rc == -1)
            goto out;
        if (rc == 0) {
            rf.is_binary = ch->is_binary;
            rf.addition = 0;
            rf.deletion = 0;
            rf.changes = 0;
            memcpy(rf.path, ch->path, MAX_PATH);
            if (insert_file(s, &rf) == -1)
                goto out;
        }

        rec_change_t rch;

        rch.commit_id = co.id;
        rch.file_id = rf.id;
        rch.addition = ch->addition;
        rch.deletion = ch->deletion;
        rch.date = cm->date;
        rch.author_id = au.id;

        /* insert change */
        if (insert_change(s, &rch) == -1)
            goto out;

        ++rf.changes;
        rf.addition += ch->addition;
        rf.deletion += ch->deletion;
        if (update_file(s, &rf) == -1)
            goto out;

        ch = ch->next;
    }

    /* update author */
    ++au.commits;
    au.changes += cm->change;
    au.addition += cm->addition;
    au.deletion += cm->deletion;
    if (update_author(s, &au) == -1)
        goto out;

    return 0;

out:
    fprintf(stderr, "Can't add commit\n");
    return -1;
}

static int
read_log(sql_serv_t *serv, char *log)
{
    FILE *fp = fopen(log, "r");

    if (!fp) {
        fprintf(stderr, "Can't open log file.\n");
        sql_serv_fini(serv);
        return -1;
    }

    sql_serv_transaction_begin(serv);
    parse_gitlog(fp, add_commit, (void *) serv);
    sql_serv_transaction_commit(serv);

    fclose(fp);

    printf("log dumped OK\n");

    return 0;
}

const char *
mkdate(time_t t)
{
    /* YYYY-MM-DD */
    static char date[11];
    struct tm *tm = localtime((const time_t *) &t);

    memset(date, 0, 11);
    strftime(date, 11, "%Y-%m-%d", (const struct tm *) tm);
    return date;
}

static void
display_rs_commit(sql_serv_t *serv, const char *name)
{
    rs_commit_t *rs = query_init_commit(serv, name);

    if (!rs)
        return;

    dump_stats_commit(rs);

    printf("============\n");
    printf("First Commit\n");
    printf("============\n");

    printf("Committer: %s\n", rs->author);
    printf("Date: %s\n", mkdate(rs->date));
    printf("Hash: %s\n", rs->hash);
    printf("Addition: %d\n", rs->addition);
    printf("Deletion: %d\n", rs->deletion);
    printf("Changes: %d\n", rs->changes);

    int i;

    printf("Files:\n");
    for (i = 0; i < rs->changes; ++i) {
        printf("%s\n", rs->files[i]);
    }

    printf("\n");

    free_rs_commit(rs);
}

static void
print_overall_stat(rs_overall_t *rs)
{
    printf("\nOverall statistics\n");
    printf("==================\n");
    printf("Commits: %d\n", rs->commits);
    printf("Changes: %d\n", rs->changes);
    printf("Files: %d\n", rs->files);
    printf("Authors: %d\n", rs->authors);
    printf("Addition: %d\n", rs->addition);
    printf("Deletion: %d\n", rs->deletion);

    printf("\nBiggest commit\n");
    printf("==============\n");
    printf("Hash: %s\n", rs->biggest_commit_hash);
    printf("Author: %s\n", rs->biggest_commit_author);
    printf("Addition: %d\n", rs->biggest_commit_addition);
    printf("Deletion: %d\n", rs->biggest_commit_deletion);

    printf("Date: %s\n", mkdate(rs->biggest_commit_time));

    int i;

#define print_stats(st) do { \
    for (i = 0; i < QUERY_LIMIT; ++i) { \
        if ((st)[i].name[0]) \
            printf("%s: %d\n", (st)[i].name, (st)[i].stat); \
    } \
} while (0)

    printf("\nTop committer of commits\n");
    printf("========================\n");
    print_stats(rs->most_commits);

    printf("\nTop committer of changes\n");
    printf("========================\n");
    print_stats(rs->most_changes);

    printf("\nTop committer of addition\n");
    printf("=========================\n");
    print_stats(rs->most_addition);

    printf("\nTop committer of deletion\n");
    printf("=========================\n");
    print_stats(rs->most_deletion);

    printf("\n");
}

/* Find season beginning of t. */
static time_t
season_time(time_t t, struct tm *tm)
{
    int season;

    localtime_r((const time_t *) &t, tm);

    season = tm->tm_mon / 3;
    tm->tm_mon = season * 3;
    tm->tm_mday = 1;

    return mktime(tm);
}

/* Find next season start time. */
static time_t
next_season(struct tm *tm)
{
    int season;

    season = tm->tm_mon / 3;
    if (season == 3) {
        ++tm->tm_year;
        tm->tm_mon = 0;
    } else {
        tm->tm_mon += 3;
    }

    return mktime(tm);
}

typedef void (*query_cb)
    (sql_serv_t *serv,
     unsigned int start,
     unsigned int end,
     int year,
     int season,
     const char *key);

static void
query_full_history(sql_serv_t *serv,
                   const char *name,
                   query_cb cb)
{
    time_t begin, end, curr;
    struct tm tm;

    query_epoch_time(serv, &begin, &end);
    curr = season_time(begin, &tm);

    do {
        unsigned int s, e;
        int year, season;

        year = tm.tm_year + 1900;
        season = tm.tm_mon / 3 + 1;

        s = curr;
        curr = next_season(&tm);
        e = curr - 1;

        cb(serv, s, e, year, season, name);
    } while (curr <= end);
}

static void
__overall_stats(sql_serv_t *serv,
                unsigned int start,
                unsigned int end,
                int year,
                int season,
                const char *unused)
{
    rs_overall_t rs;

    unused = unused;

    if (query_overall(serv, start, end, &rs) == -1)
        return;

    dump_stats_overall(year, season, &rs);

    if (year && season) {
        printf("=======================\n");
        printf("Year: %d Season: Q%d\n", year, season);
        printf("=======================\n");
    }
    print_overall_stat(&rs);
}

static void
overall_stats(sql_serv_t *serv,
              unsigned int start,
              unsigned int end,
              const char *unused)
{
    dump_stats_overall_header();

    if (start == -1 || end == -1) {
        query_full_history(serv, unused, __overall_stats);
        printf("==================\n");
        printf("Overall statistics\n");
        printf("==================\n");
        __overall_stats(serv, 0, INT_MAX, 0, 0, unused);
    } else {
        __overall_stats(serv, start, end, 0, 0, unused);
    }

    dump_stats_overall_tail();

    display_rs_commit(serv, NULL);
}

static int
str2epoch(const char *arg)
{
    struct tm tm;

    memset(&tm, 0, sizeof(tm));
    if (strptime(arg, "%Y-%m-%d", &tm) == NULL)
        return -1;

    return mktime(&tm);
}

static void
__query_committer(sql_serv_t *serv,
                  unsigned int start,
                  unsigned int end,
                  int year,
                  int season,
                  const char *name)
{
    int rc;
    rs_author_t rs;

    if ((rc = query_author(serv, start, end, name, &rs)) == -1)
        return;

    dump_stats_committer(year, season, &rs);

    if (year && season) {
        printf("=======================\n");
        printf("Year: %d Season: Q%d\n", year, season);
        printf("=======================\n");
    }

    printf("\nCommitter statistics\n");
    printf("====================\n");
    printf("%s\n", name);
    printf("====================\n");
    printf("Commits: %d\n", rs.commits);
    printf("Changes: %d\n", rs.changes);
    printf("Addition: %d\n", rs.addition);
    printf("Deletion: %d\n", rs.deletion);

    int i;

    printf("\nTop files of addition\n");
    printf("=====================\n");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs.most_addition[i].name[0])
            printf("%s: %d\n", rs.most_addition[i].name, rs.most_addition[i].stat);
    }

    printf("\nTop files of change\n");
    printf("===================\n");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs.most_change[i].name[0])
            printf("%s: %d\n", rs.most_change[i].name, rs.most_change[i].stat);
    }

    printf("\n");
}


static void
query_committer(sql_serv_t *serv,
                unsigned int start,
                unsigned int end,
                const char *name)
{
    dump_stats_committer_head();

    if (start == -1 || end == -1) {
        query_full_history(serv, name, __query_committer);
        printf("====================\n");
        printf("Overall of %s\n", name);
        printf("====================\n");
        __query_committer(serv, 0, INT_MAX, 0, 0, name);
    } else {
        __query_committer(serv, start, end, 0, 0, name);
    }

    dump_stats_committer_tail();

    display_rs_commit(serv, name);
}

static void
__query_path(sql_serv_t *serv,
             unsigned int start,
             unsigned int end,
             int year,
             int season,
             const char *name)
{
    int rc;
    rs_file_t rs;

    if ((rc = query_file(serv, start, end, name, &rs)) == -1)
        return;

    dump_stats_path(year, season, &rs);

    if (year && season) {
        printf("=======================\n");
        printf("Year: %d Season: Q%d\n", year, season);
        printf("=======================\n");
    }

    printf("\nPath statistics\n");
    printf("====================\n");
    printf("%s\n", name);
    printf("====================\n");
    printf("Authors: %d\n", rs.authors);
    printf("Commits: %d\n", rs.commits);
    printf("Changes: %d\n", rs.changes);
    printf("Addition: %d\n", rs.addition);
    printf("Deletion: %d\n", rs.deletion);
    printf("Files: %d\n", rs.files);

    int i;

    printf("\nTop committer of addition\n");
    printf("=========================\n");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs.top_committers[i].name[0])
            printf("%s: %d\n", rs.top_committers[i].name, rs.top_committers[i].stat);
    }

    printf("\nTop files of change\n");
    printf("===================\n");
    for (i = 0; i < QUERY_LIMIT; ++i) {
        if (rs.top_changes[i].name[0])
            printf("%s: %d\n", rs.top_changes[i].name, rs.top_changes[i].stat);
    }

    printf("\n");
}

static void
query_path(sql_serv_t *serv,
           unsigned int start,
           unsigned int end,
           const char *name)
{
    dump_stats_path_head();

    if (start == -1 || end == -1) {
        query_full_history(serv, name, __query_path);
        printf("====================\n");
        printf("Overall of %s\n", name);
        printf("====================\n");
        __query_path(serv, 0, INT_MAX, 0, 0, name);
    } else {
        __query_path(serv, start, end, 0, 0, name);
    }

    dump_stats_path_tail();
}

static void
usage(void)
{
    printf("Usage: gitlog [OPTION]\n");
    printf("Read git log from plain text and store in databse.\n");
    printf("Query git log info from database.\n\n");
    printf("  -s            start date. format YYYY-M-D. e.g. 2000-12-3.\n");
    printf("  -e            end date.\n");
    printf("  -i            git log text file path.\n");
    printf("                use git command:       \n");
    printf("                  git log --no-merges --format=\"%%n%%H::%%cn::%%ct\" --numstat\n");
    printf("  -o            git log database file path.\n");
    printf("  -n            query for given committer name.\n");
    printf("  -p            query for given path.\n");
    printf("  -g            path to dump statistics in visual format.\n");
    printf("  -h            display this help info.\n\n");
    exit(0);
}

int
main(int argc, char **argv)
{
    int start, end, opt;
    int create;
    char *log = NULL;
    char *db = NULL;
    char *committer = NULL;
    char *path = NULL;
    char *dump_path = NULL;

    start = end = -1;

    while ((opt = getopt(argc, argv, "s:e:i:o:n:p:hg:")) != -1) {
        switch (opt) {
        case 's':
            if (optarg[0] == '0') {
                start = 0;
            } else {
                start = str2epoch(optarg);
            }
            break;

        case 'e':
            if (optarg[0] == '0') {
                end = INT_MAX;
            } else {
                end = str2epoch(optarg);
            }
            break;

        case 'i':
            log = strdup(optarg);
            break;

        case 'o':
            db = strdup(optarg);
            break;

        case 'g':
            dump_path = strdup(optarg);
            break;

        case 'n':
            committer = strdup(optarg);
            break;

        case 'h':
            usage();
            break;

        case 'p':
            path = strdup(optarg);
            break;

        default:
            break;
        }
    }

    /* db must be given */
    if (!db)
        usage();

    /* if log is given then work in create mode */
    if (log)
        create = 1;
    else
        create = 0;

    if (create == 0) {
        if (dump_path) {
            dump_stats_init(dump_path);
        }
    }

    sql_serv_t *serv = sql_serv_init(create, db);

    if (!serv)
        return -1;

    if (create) {
        printf("Dumping logs...\n");
        read_log(serv, log);
    } else {
        if (committer) {
            query_committer(serv, start, end, committer);
        } else if (path) {
            query_path(serv, start, end, path);
        } else {
            overall_stats(serv, start, end, NULL);
        }
    }

    sql_serv_fini(serv);

    if (log)
        free(log);
    if (db)
        free(db);
    if (committer)
        free(committer);
    if (path)
        free(path);
    if (dump_path) {
        dump_stats_fini();
        free(dump_path);
    }

    return 0;
}

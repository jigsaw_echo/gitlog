#ifndef _CONFIG_H
#define _CONFIG_H

#define MAX_PATH 512
#define MAX_HASH 64
#define MAX_AUTHOR 128
#define MAX_EMAIL 128
#define MAX_STRING MAX_PATH


#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

#include "sql_serv.h"

struct sql_server {
#define SQL_MODE_WRITE 0
#define SQL_MODE_READ  1
    int mode;

    sqlite3 *db;

    sqlite3_stmt *search_author;
    sqlite3_stmt *insert_author;
    sqlite3_stmt *update_author;
    sqlite3_stmt *search_file;
    sqlite3_stmt *insert_file;
    sqlite3_stmt *update_file;
    sqlite3_stmt *insert_commit;
    sqlite3_stmt *insert_change;

    sqlite3_stmt *stats_query;
    sqlite3_stmt *most_commits;
    sqlite3_stmt *most_changes;
    sqlite3_stmt *most_addition;
    sqlite3_stmt *most_deletion;
    sqlite3_stmt *biggest_commit;
    sqlite3_stmt *epoch;
    sqlite3_stmt *committer_query;
    sqlite3_stmt *file_query;
    sqlite3_stmt *file_author_query;
    sqlite3_stmt *file_change_query;
    sqlite3_stmt *committer_addition_query;
    sqlite3_stmt *committer_change_query;
    sqlite3_stmt *init_commit_for_author;
    sqlite3_stmt *init_commit;
    sqlite3_stmt *commit_files;
};

/*

+------------+     +------------+     +------------+      +------------+
|   author   |     |   commits  |     |  change    |      |    file    |
+------------+     +------------+     +------------+      +------------+
|     id     |<-+  |     id     |<-+  |     id     |   +->|     id     |
+------------+  |  +------------+  |  +------------+   |  +------------+
|    name    |  +--|  author_id |  +--|  commit_id |   |  |  is_binary |
+------------+  |  +------------+     +------------+   |  +------------+
|  commits   |  |  |    hash    |     |  file_id   |---+  |    path    |
+------------+  |  +------------+     +------------+      +------------+
|  changes   |  |  |    date    |     |  addition  |      |  addition  |
+------------+  |  +------------+     +------------+      +------------+
|  addition  |  |  |   changes  |     |  deletion  |      |  deletion  |
+------------+  |  +------------+     +------------+      +------------+
|  deletion  |  |  |  addition  |     |    date    |      |  changes   |
+------------+  |  +------------+     +------------+      +------------+
                |  |  deletion  |  +--|  author_id |
                |  +------------+  |  +------------+
                |                  |
                +------------------+

*/

#define reset_stmt(__s) do { \
    sqlite3_clear_bindings(__s); \
    sqlite3_reset(__s); \
} while (0)

static int
create_tables(sqlite3 *db)
{
    static const char *file_table =
        "CREATE TABLE IF NOT EXISTS file ("
        "id INTEGER PRIMARY KEY,"
        "is_binary INTEGER,"
        "path TEXT,"
        "addition INTEGER,"
        "deletion INTEGER,"
        "changes INTEGER"
        ");";

    static const char *author_table =
        "CREATE TABLE IF NOT EXISTS author ("
        "id INTEGER PRIMARY KEY,"
        "name TEXT,"
        "commits INTEGER,"
        "changes INTEGER,"
        "addition INTEGER,"
        "deletion INTEGER"
        ");";

    static const char *commit_table =
        "CREATE TABLE IF NOT EXISTS commits ("
        "id INTEGER PRIMARY KEY,"
        "author_id INTEGER,"
        "hash TEXT,"
        "date INTEGER,"
        "changes INTEGER,"
        "addition INTEGER,"
        "deletion INTEGER,"
        "FOREIGN KEY(author_id) REFERENCES author(id)"
        ");";

    static const char *change_table =
        "CREATE TABLE IF NOT EXISTS change ("
        "id INTEGER PRIMARY KEY,"
        "commit_id INTEGER,"
        "file_id INTEGER,"
        "author_id INTEGER,"
        "addition INTEGER,"
        "deletion INTEGER,"
        "date INTEGER,"
        "FOREIGN KEY(commit_id) REFERENCES commits(id),"
        "FOREIGN KEY(file_id) REFERENCES file(id),"
        "FOREIGN KEY(author_id) REFERENCES author(id)"
        ");";

    int i, rc;
    const char *tables[] =
        {file_table, author_table, commit_table, change_table};

    for (i = 0; i < 4; ++i)
        if ((rc = sqlite3_exec(db, tables[i], NULL, NULL, NULL)))
            return rc;

    return 0;
}

void
sql_serv_fini(sql_serv_t *serv)
{
    if (serv->mode == SQL_MODE_WRITE) {
        sqlite3_finalize(serv->search_author);
        sqlite3_finalize(serv->insert_author);
        sqlite3_finalize(serv->update_author);
        sqlite3_finalize(serv->search_file);
        sqlite3_finalize(serv->insert_file);
        sqlite3_finalize(serv->update_file);
        sqlite3_finalize(serv->insert_commit);
        sqlite3_finalize(serv->insert_change);
    } else {
        sqlite3_finalize(serv->stats_query);
        sqlite3_finalize(serv->most_commits);
        sqlite3_finalize(serv->most_changes);
        sqlite3_finalize(serv->most_addition);
        sqlite3_finalize(serv->most_deletion);
        sqlite3_finalize(serv->biggest_commit);
        sqlite3_finalize(serv->epoch);
        sqlite3_finalize(serv->committer_query);
        sqlite3_finalize(serv->committer_addition_query);
        sqlite3_finalize(serv->committer_change_query);
        sqlite3_finalize(serv->file_query);
        sqlite3_finalize(serv->file_author_query);
        sqlite3_finalize(serv->file_change_query);
        sqlite3_finalize(serv->init_commit_for_author);
        sqlite3_finalize(serv->init_commit);
        sqlite3_finalize(serv->commit_files);
    }

    sqlite3_close(serv->db);
    free(serv);
}

static int
prepare_stmts_write(sql_serv_t *serv)
{
    static const char *search_author_q =
        "SELECT id, commits, changes, addition, deletion FROM author "
        "WHERE name = ?;";
    static const char *insert_author_q =
        "INSERT INTO author (name, commits, changes, addition, deletion) "
        "VALUES (?, 0, 0, 0, 0);";
    static const char *update_author_q =
        "UPDATE author SET commits = ?, changes = ?, addition = ?, deletion = ? "
        "WHERE id = ?;";
    static const char *search_file_q = 
        "SELECT id, is_binary, addition, deletion, changes FROM file "
        "where path = ?;";
    static const char *insert_file_q =
        "INSERT INTO file (is_binary, path, addition, deletion, changes) "
        "VALUES (?, ?, 0, 0, 0);";
    static const char *update_file_q =
        "UPDATE file SET addition = ?, deletion = ?, changes = ? "
        "WHERE id = ?;";
    static const char *insert_commit_q =
        "INSERT INTO commits (author_id, hash, date, changes, addition, deletion) "
        "VALUES (?, ?, ?, ?, ?, ?);";
    static const char *insert_change_q =
        "INSERT INTO change (commit_id, file_id, author_id, addition, deletion, date) "
        "VALUES (?, ?, ?, ?, ?, ?);";

    const struct {
        const char *sql;
        sqlite3_stmt **st;
    } tmp_stmts[] = {
        {search_author_q, &serv->search_author},
        {insert_author_q, &serv->insert_author},
        {update_author_q, &serv->update_author},
        {search_file_q,   &serv->search_file},
        {insert_file_q,   &serv->insert_file},
        {update_file_q,   &serv->update_file},
        {insert_commit_q, &serv->insert_commit},
        {insert_change_q, &serv->insert_change},
    };

    int i;

    for (i = 0; i < 8; ++i) {
        if (sqlite3_prepare_v2(serv->db,
                    tmp_stmts[i].sql,
                    -1,
                    tmp_stmts[i].st, NULL) != SQLITE_OK) {
            fprintf(stderr, "Can't prepare stmt: %s : %s\n", tmp_stmts[i].sql, sqlite3_errmsg(serv->db));
            return -1;
        }
    }

    return 0;
}

static int
prepare_stmts_read(sql_serv_t *serv)
{
    static const char *stats_query_q =
        "SELECT COUNT(DISTINCT change.commit_id), COUNT(DISTINCT change.id), COUNT(DISTINCT change.author_id), "
        "SUM(change.addition), SUM(change.deletion), COUNT(DISTINCT change.file_id) FROM change "
        "WHERE change.date >= ? AND change.date <= ?;";

    static const char *most_commits_q =
        "SELECT author.name, COUNT(commits.id) AS c "
        "FROM author JOIN commits ON author.id = commits.author_id "
        "WHERE commits.date >= ? AND commits.date <= ? "
        "GROUP BY author.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *most_changes_q =
        "SELECT author.name, COUNT(change.id) AS c "
        "FROM author JOIN change ON author.id = change.author_id "
        "WHERE change.date >= ? AND change.date <= ? "
        "GROUP BY author.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *most_addition_q =
        "SELECT author.name, SUM(change.addition) AS c "
        "FROM author JOIN change ON author.id = change.author_id "
        "WHERE change.date >= ? AND change.date <= ? "
        "GROUP BY author.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *most_deletion_q =
        "SELECT author.name, SUM(change.deletion) AS c "
        "FROM author JOIN change ON author.id = change.author_id "
        "WHERE change.date >= ? AND change.date <= ? "
        "GROUP BY author.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *biggest_commit_q =
        "SELECT commits.hash, author.name, MAX(commits.addition), commits.date, commits.deletion "
        "FROM commits JOIN author ON author.id = commits.author_id "
        "WHERE commits.date >= ? AND commits.date <= ?;";

    static const char *epoch_q =
        "SELECT MIN(commits.date), MAX(commits.date) FROM commits;";

    static const char *committer_query_q =
        "SELECT COUNT(DISTINCT change.commit_id), COUNT(DISTINCT change.id), "
        "SUM(change.addition), SUM(change.deletion) FROM change "
        "JOIN author ON author.id = change.author_id "
        "WHERE change.date >= ? AND change.date <= ? AND author.name = ?;";

    static const char *committer_addition_query_q =
        "SELECT file.path, SUM(change.addition) AS c "
        "FROM change JOIN author ON change.author_id = author.id "
        "JOIN file ON change.file_id = file.id "
        "WHERE author.name = ? "
        "AND change.date >= ? AND change.date <= ? "
        "GROUP BY file.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *committer_change_query_q =
        "SELECT file.path, COUNT(DISTINCT change.id) AS c "
        "FROM change JOIN author ON change.author_id = author.id "
        "JOIN file ON change.file_id = file.id "
        "WHERE author.name = ? "
        "AND change.date >= ? AND change.date <= ? "
        "GROUP BY file.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *file_query_q =
        "SELECT COUNT(DISTINCT change.author_id), COUNT(DISTINCT change.commit_id), "
        "COUNT(DISTINCT change.id), SUM(change.addition), SUM(change.deletion), COUNT(DISTINCT file.id) "
        "FROM change JOIN file ON file.id = change.file_id "
        "WHERE change.date >= ? AND change.date <= ? "
        "AND file.path LIKE ?;";

    static const char *file_author_query_q =
        "SELECT author.name, SUM(change.addition) AS c "
        "FROM change JOIN author ON change.author_id = author.id "
        "JOIN file ON file.id = change.file_id "
        "WHERE change.date >= ? AND change.date <= ? "
        "AND file.path LIKE ? "
        "GROUP BY author.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *file_change_query_q =
        "SELECT file.path, COUNT(DISTINCT change.id) AS c "
        "FROM change JOIN file ON file.id = change.file_id "
        "WHERE change.date >= ? AND change.date <= ? "
        "AND file.path LIKE ? "
        "GROUP BY file.id ORDER BY c DESC LIMIT " QUERY_LIMIT_S ";";

    static const char *init_commit_for_author_q =
        "SELECT commits.id, MIN(commits.date), author.name, commits.hash, "
        "commits.addition, commits.deletion, commits.changes "
        "FROM commits JOIN author ON commits.author_id = author.id "
        "where author.name = ?;";

    static const char *init_commit_q =
        "SELECT commits.id, MIN(commits.date), author.name, commits.hash, "
        "commits.addition, commits.deletion, commits.changes "
        "FROM commits JOIN author ON commits.author_id = author.id;";

    static const char *commit_files_q =
        "SELECT file.path FROM file "
        "JOIN change ON change.file_id = file.id "
        "WHERE change.commit_id = ?;";

    const struct {
        const char *sql;
        sqlite3_stmt **st;
    } tmp_stmts[] = {
        {stats_query_q,   &serv->stats_query},
        {most_commits_q,  &serv->most_commits},
        {most_changes_q,  &serv->most_changes},
        {most_addition_q, &serv->most_addition},
        {most_deletion_q, &serv->most_deletion},
        {biggest_commit_q, &serv->biggest_commit},
        {epoch_q, &serv->epoch},
        {committer_query_q, &serv->committer_query},
        {committer_addition_query_q, &serv->committer_addition_query},
        {committer_change_query_q, &serv->committer_change_query},
        {file_query_q, &serv->file_query},
        {file_author_query_q, &serv->file_author_query},
        {file_change_query_q, &serv->file_change_query},
        {init_commit_for_author_q, &serv->init_commit_for_author},
        {init_commit_q, &serv->init_commit},
        {commit_files_q, &serv->commit_files},
    };

    int i;

    for (i = 0; i < 16; ++i) {
        if (sqlite3_prepare_v2(serv->db,
                    tmp_stmts[i].sql,
                    -1,
                    tmp_stmts[i].st, NULL) != SQLITE_OK) {
            fprintf(stderr, "Can't prepare stmt: %s : %s\n", tmp_stmts[i].sql, sqlite3_errmsg(serv->db));
            return -1;
        }
    }

    return 0;
}

static int
prepare_stmts(sql_serv_t *serv)
{
    if (serv->mode == SQL_MODE_WRITE)
        return prepare_stmts_write(serv);
    else
        return prepare_stmts_read(serv);
}

sql_serv_t *
sql_serv_init(int create, char *db_path)
{
    sqlite3 *db;
    sql_serv_t *serv;
    int rc;

    rc = sqlite3_open(db_path, &db);
    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return NULL;
    }

    /* create tables */
    if (create) {
        rc = create_tables(db);
        if (rc) {
            fprintf(stderr, "Can't create tables: %s\n", sqlite3_errmsg(db));
            sqlite3_close(db);
            return NULL;
        }
    }

    sqlite3_exec(db, "PRAGMA synchronous = OFF", NULL, NULL, NULL);
    sqlite3_exec(db, "PRAGMA journal_mode = MEMORY", NULL, NULL, NULL);

    serv = malloc(sizeof(*serv));
    serv->db = db;
    serv->mode = create ? SQL_MODE_WRITE : SQL_MODE_READ;

    if (prepare_stmts(serv) == -1) {
        sqlite3_close(db);
        free(serv);
        return NULL;
    }

    return serv;
}

int
sql_serv_transaction_begin(sql_serv_t *serv)
{
    if (sqlite3_exec(serv->db, "BEGIN TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "Can't BEGIN transcation\n");
        sqlite3_close(serv->db);
        return -1;
    }

    return 0;
}

int
sql_serv_transaction_commit(sql_serv_t *serv)
{
    if (sqlite3_exec(serv->db, "END TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "Can't COMMIT transcation\n");
        sqlite3_close(serv->db);
        return -1;
    }

    return 0;
}

int
lookup_author(sql_serv_t *serv, const char *name, rec_author_t *rec)
{
    int found = 0;
    int rc;

    if ((rc = sqlite3_bind_text(serv->search_author,
                          1,
                          name,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->search_author) == SQLITE_ROW)) {
        found = 1;
        rec->id = sqlite3_column_int(serv->search_author, 0);
        rec->commits = sqlite3_column_int(serv->search_author, 1);
        rec->changes = sqlite3_column_int(serv->search_author, 2);
        rec->addition = sqlite3_column_int(serv->search_author, 3);
        rec->deletion = sqlite3_column_int(serv->search_author, 4);
    }

    reset_stmt(serv->search_author);

    return found;
}

int
insert_author(sql_serv_t *serv, rec_author_t *rec)
{
    int rc;

    if ((rc = sqlite3_bind_text(serv->insert_author,
                          1,
                          rec->name,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->insert_author)) != SQLITE_DONE)
        return -1;

    reset_stmt(serv->insert_author);
    rec->id = sqlite3_last_insert_rowid(serv->db);

    return 0;
}

int
update_author(sql_serv_t *serv, rec_author_t *rec)
{
    int rc;

    if ((rc = sqlite3_bind_int(serv->update_author,
                         1,
                         rec->commits)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_author,
                         2,
                         rec->changes)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_author,
                         3,
                         rec->addition)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_author,
                         4,
                         rec->deletion)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_author,
                         5,
                         rec->id)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->update_author)) != SQLITE_DONE)
        return -1;

    reset_stmt(serv->update_author);

    return 0;
}

int
lookup_file(sql_serv_t *serv, char *path, rec_file_t *rec)
{
    int found = 0;
    int rc;

    if ((rc = sqlite3_bind_text(serv->search_file,
                          1,
                          path,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->search_file)) == SQLITE_ROW) {
        found = 1;
        rec->id = sqlite3_column_int(serv->search_file, 0);
        rec->is_binary = sqlite3_column_int(serv->search_file, 1);
        rec->addition = sqlite3_column_int(serv->search_file, 2);
        rec->deletion = sqlite3_column_int(serv->search_file, 3);
        rec->changes = sqlite3_column_int(serv->search_file, 4);
    }
    reset_stmt(serv->search_file);

    return found;
}

int
insert_file(sql_serv_t *serv, rec_file_t *rec)
{
    int rc;

    if ((rc = sqlite3_bind_int(serv->insert_file,
                         1,
                         rec->is_binary)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_text(serv->insert_file,
                          2,
                          rec->path,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->insert_file)) != SQLITE_DONE)
        return -1;

    reset_stmt(serv->insert_file);
    rec->id = sqlite3_last_insert_rowid(serv->db);

    return 0;
}

int
update_file(sql_serv_t *serv, rec_file_t *rec)
{
    int rc;

    if ((rc = sqlite3_bind_int(serv->update_file,
                         1,
                         rec->addition)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_file,
                         2,
                         rec->deletion)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_file,
                         3,
                         rec->changes)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->update_file,
                         4,
                         rec->id)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->update_file)) != SQLITE_DONE)
        return -1;

    reset_stmt(serv->update_file);

    return 0;
}

int
insert_commit(sql_serv_t *serv, rec_commit_t *rec)
{
    int rc;

    if ((rc = sqlite3_bind_int(serv->insert_commit,
                         1,
                         rec->author_id)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_text(serv->insert_commit,
                          2,
                          rec->hash,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_commit,
                         3,
                         rec->date)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_commit,
                         4,
                         rec->changes)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_commit,
                         5,
                         rec->addition)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_commit,
                         6,
                         rec->deletion)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->insert_commit)) != SQLITE_DONE)
        return -1;

    reset_stmt(serv->insert_commit);
    rec->id = sqlite3_last_insert_rowid(serv->db);

    return 0;
}

int
insert_change(sql_serv_t *serv, rec_change_t *rec)
{
    int rc;

    if ((rc = sqlite3_bind_int(serv->insert_change,
                         1,
                         rec->commit_id)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_change,
                         2,
                         rec->file_id)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_change,
                         3,
                         rec->author_id)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_change,
                         4,
                         rec->addition)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_change,
                         5,
                         rec->deletion)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->insert_change,
                         6,
                         rec->date)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->insert_change)) != SQLITE_DONE)
        return -1;

    reset_stmt(serv->insert_change);
    rec->id = sqlite3_last_insert_rowid(serv->db);

    return 0;
}

int
query_overall(sql_serv_t *serv,
          unsigned int start,
          unsigned int end,
          rs_overall_t *rs)
{
    int rc;

    if (end <= start)
        return -1;

    memset(rs, 0, sizeof(*rs));

    if ((rc = sqlite3_bind_int(serv->stats_query,
                          1,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->stats_query,
                          2,
                          end) != SQLITE_OK))
        return -1;

    if ((rc = sqlite3_step(serv->stats_query)) == SQLITE_ROW) {
        rs->commits = sqlite3_column_int(serv->stats_query, 0);
        rs->changes = sqlite3_column_int(serv->stats_query, 1);
        rs->authors = sqlite3_column_int(serv->stats_query, 2);
        rs->addition = sqlite3_column_int(serv->stats_query, 3);
        rs->deletion = sqlite3_column_int(serv->stats_query, 4);
        rs->files = sqlite3_column_int(serv->stats_query, 5);
    }

    reset_stmt(serv->stats_query);

    if (rs->commits == 0)
        return -1;

    /* biggest commit */
    if ((rc = sqlite3_bind_int(serv->biggest_commit,
                          1,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->biggest_commit,
                          2,
                          end) != SQLITE_OK))
        return -1;

    if ((rc = sqlite3_step(serv->biggest_commit)) == SQLITE_ROW) {
        const char *hash = (const char *) sqlite3_column_text(serv->biggest_commit, 0);
        strncpy(rs->biggest_commit_hash, hash, MAX_HASH);
        const char *name = (const char *) sqlite3_column_text(serv->biggest_commit, 1);
        strncpy(rs->biggest_commit_author, name, MAX_AUTHOR);
        rs->biggest_commit_addition = sqlite3_column_int(serv->biggest_commit, 2);
        rs->biggest_commit_time = sqlite3_column_int(serv->biggest_commit, 3);
        rs->biggest_commit_deletion = sqlite3_column_int(serv->biggest_commit, 4);
    }

    reset_stmt(serv->biggest_commit);

    /* list authors */
    struct {
        rs_top_t *au[QUERY_LIMIT];
        sqlite3_stmt *st;
    } tmp_stmts[] = {
        {.st = serv->most_commits},
        {.st = serv->most_changes},
        {.st = serv->most_addition},
        {.st = serv->most_deletion},
    };

    int i;

    for (i = 0; i < QUERY_LIMIT; ++i) {
        tmp_stmts[0].au[i] = &rs->most_commits[i];
        tmp_stmts[1].au[i] = &rs->most_changes[i];
        tmp_stmts[2].au[i] = &rs->most_addition[i];
        tmp_stmts[3].au[i] = &rs->most_deletion[i];
    }

    for (i = 0; i < 4; ++i) {
        int j = 0;
        sqlite3_stmt *stmt = tmp_stmts[i].st;

        if ((rc = sqlite3_bind_int(stmt,
                        1,
                        start) != SQLITE_OK))
            return -1;
        if ((rc = sqlite3_bind_int(stmt,
                        2,
                        end) != SQLITE_OK))
            return -1;

        while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
            const char *name = (const char *) sqlite3_column_text(stmt, 0);
            int st = sqlite3_column_int(stmt, 1);

            strncpy(tmp_stmts[i].au[j]->name, name, MAX_AUTHOR);
            tmp_stmts[i].au[j++]->stat = st;
        }
        reset_stmt(stmt);
    }

    return 0;
}

void
query_epoch_time(sql_serv_t *serv, time_t *begin, time_t *end)
{
    int rc;

    if ((rc = sqlite3_step(serv->epoch)) == SQLITE_ROW) {
        int t0 = sqlite3_column_int(serv->epoch, 0);
        int t1 = sqlite3_column_int(serv->epoch, 1);
        *begin = t0;
        *end = t1;
    }

    reset_stmt(serv->epoch);
}

int
query_author(sql_serv_t *serv,
              unsigned int start,
              unsigned int end,
              const char *name,
              rs_author_t *rs)
{
    int rc;

    if (end <= start)
        return -1;

    memset(rs, 0, sizeof(*rs));

    if ((rc = sqlite3_bind_int(serv->committer_query,
                          1,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->committer_query,
                          2,
                          end) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_text(serv->committer_query,
                          3,
                          name,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->committer_query)) == SQLITE_ROW) {
        rs->commits = sqlite3_column_int(serv->committer_query, 0);
        rs->changes = sqlite3_column_int(serv->committer_query, 1);
        rs->addition = sqlite3_column_int(serv->committer_query, 2);
        rs->deletion = sqlite3_column_int(serv->committer_query, 3);
        rc = 0;
    } else {
        rc = -1;
    }

    reset_stmt(serv->committer_query);

    if (rs->commits == 0)
        return -1;

    if ((rc = sqlite3_bind_text(serv->committer_addition_query,
                          1,
                          name,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->committer_addition_query,
                          2,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->committer_addition_query,
                          3,
                          end) != SQLITE_OK))
        return -1;

    int i = 0;

    while ((rc = sqlite3_step(serv->committer_addition_query)) == SQLITE_ROW) {
        const char *name = (const char *) sqlite3_column_text(serv->committer_addition_query, 0);
        int st = sqlite3_column_int(serv->committer_addition_query, 1);

        strncpy(rs->most_addition[i].name, name, MAX_AUTHOR);
        rs->most_addition[i++].stat = st;
    }

    reset_stmt(serv->committer_addition_query);

    if ((rc = sqlite3_bind_text(serv->committer_change_query,
                          1,
                          name,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;
    if ((rc = sqlite3_bind_int(serv->committer_change_query,
                          2,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->committer_change_query,
                          3,
                          end) != SQLITE_OK))
        return -1;

    i = 0;

    while ((rc = sqlite3_step(serv->committer_change_query)) == SQLITE_ROW) {
        const char *name = (const char *) sqlite3_column_text(serv->committer_change_query, 0);
        int st = sqlite3_column_int(serv->committer_change_query, 1);

        strncpy(rs->most_change[i].name, name, MAX_AUTHOR);
        rs->most_change[i++].stat = st;
    }

    reset_stmt(serv->committer_change_query);
    return rc;
}

int
query_file(sql_serv_t *serv,
           unsigned int start,
           unsigned int end,
           const char *name,
           rs_file_t *rs)
{
    int rc, len;
    char path[MAX_PATH + 1];

    if (end <= start)
        return -1;

    memset(rs, 0, sizeof(*rs));

    len = strlen(name);
    strncpy(path, name, MAX_PATH);
    path[len] = '%';
    path[len + 1] = 0;

    if ((rc = sqlite3_bind_int(serv->file_query,
                          1,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->file_query,
                          2,
                          end) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_text(serv->file_query,
                          3,
                          path,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    if ((rc = sqlite3_step(serv->file_query)) == SQLITE_ROW) {
        rs->authors = sqlite3_column_int(serv->file_query, 0);
        rs->commits = sqlite3_column_int(serv->file_query, 1);
        rs->changes = sqlite3_column_int(serv->file_query, 2);
        rs->addition = sqlite3_column_int(serv->file_query, 3);
        rs->deletion = sqlite3_column_int(serv->file_query, 4);
        rs->files = sqlite3_column_int(serv->file_query, 5);
        rc = 0;
    } else {
        rc = -1;
    }

    reset_stmt(serv->file_query);

    if (rs->commits == 0)
        return -1;

    if ((rc = sqlite3_bind_int(serv->file_author_query,
                          1,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->file_author_query,
                          2,
                          end) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_text(serv->file_author_query,
                          3,
                          path,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    int i = 0;

    while ((rc = sqlite3_step(serv->file_author_query)) == SQLITE_ROW) {
        const char *name = (const char *) sqlite3_column_text(serv->file_author_query, 0);
        int st = sqlite3_column_int(serv->file_author_query, 1);

        strncpy(rs->top_committers[i].name, name, MAX_AUTHOR);
        rs->top_committers[i++].stat = st;
    }

    reset_stmt(serv->file_author_query);

    if ((rc = sqlite3_bind_int(serv->file_change_query,
                          1,
                          start) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_int(serv->file_change_query,
                          2,
                          end) != SQLITE_OK))
        return -1;
    if ((rc = sqlite3_bind_text(serv->file_change_query,
                          3,
                          path,
                          -1,
                          SQLITE_TRANSIENT)) != SQLITE_OK)
        return -1;

    i = 0;

    while ((rc = sqlite3_step(serv->file_change_query)) == SQLITE_ROW) {
        const char *name = (const char *) sqlite3_column_text(serv->file_change_query, 0);
        int st = sqlite3_column_int(serv->file_change_query, 1);

        strncpy(rs->top_changes[i].name, name, MAX_AUTHOR);
        rs->top_changes[i++].stat = st;
    }

    reset_stmt(serv->file_change_query);

    return rc;
}

rs_commit_t *
query_init_commit(sql_serv_t *serv, const char *name)
{
    int rc;
    sqlite3_stmt *stmt;
    rs_commit_t rs;
    int commit_id;

    memset(&rs, 0, sizeof(rs));

    if (name) {
        stmt = serv->init_commit_for_author;
        if ((rc = sqlite3_bind_text(stmt,
                        1,
                        name,
                        -1,
                        SQLITE_TRANSIENT)) != SQLITE_OK)
            return NULL;
    } else {
        stmt = serv->init_commit;
    }

    rc = -1;

    if ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        commit_id = sqlite3_column_int(stmt, 0);
        rs.date = sqlite3_column_int(stmt, 1);

        const char *au = (const char *) sqlite3_column_text(stmt, 2);

        if (au) {
            strncpy(rs.author, au, MAX_AUTHOR);

            const char *hash = (const char *) sqlite3_column_text(stmt, 3);
            strncpy(rs.hash, hash, MAX_HASH);

            rs.addition = sqlite3_column_int(stmt, 4);
            rs.deletion = sqlite3_column_int(stmt, 5);
            rs.changes = sqlite3_column_int(stmt, 6);
        } else
            rc = -1;
    }

    reset_stmt(stmt);

    if (rc == -1)
        return NULL;

    rs_commit_t *rs2 = malloc(sizeof(*rs2) + sizeof(char *) * rs.changes);

    stmt = serv->commit_files;

    if ((rc = sqlite3_bind_int(stmt,
                          1,
                          commit_id) != SQLITE_OK)) {
        free(rs2);
        return NULL;
    }

    memcpy(rs2, &rs, sizeof(rs));

    int i = 0;

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        const char *f = (const char *) sqlite3_column_text(stmt, 0);
        rs2->files[i++] = strdup(f);
    }

    reset_stmt(stmt);

    return rs2;
}

void
free_rs_commit(rs_commit_t *rs)
{
    int i;

    for (i = 0; i < rs->changes; ++i) {
        free(rs->files[i]);
    }

    free(rs);
}
